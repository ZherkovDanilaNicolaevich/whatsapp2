/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.student;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import javax.persistence.Entity;

/**
 *
 * @author developer
 */
@Entity
public class Student extends PanacheEntity {
    
    public String firstName;        // Имя 
    public String sureName;         // Фамилия
    public String lastName;       // Отчество
    public String course;         // Курс 
    public String studentGroup;     // Студенческая группа
    
}
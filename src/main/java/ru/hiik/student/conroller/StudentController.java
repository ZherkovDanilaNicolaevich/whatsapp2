/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.student.conroller;

/**
 *
 * @author student
 */
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ru.hiik.student.Student;
import ru.hiik.student.service.StudentService;

//   10.0.0.10:9797/students 
//   localhost:9797/students  

@Path("/students")
public class StudentController {

    private static final Logger log = Logger.getLogger(StudentController.class.getName());
    
   
    @Inject
    StudentService service;
    
     
    /**
     * Получение полного списка студентов
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)   // Преобразует список в JSON массив
    public List<Student> getAllStudent() {
        log.log(Level.INFO, "Поступил запрос на получение полного списка студентов");
        List<Student> students = service.getAll();
        if (students != null) {
            log.log(Level.INFO, "Получен список студентов [" + students.size() + "] записей");
        }

        return students;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    
    public Student save (Student student) {
        log.log(Level.INFO, "Поступил запрос на сохранение студента в базе данных");
        Student saveStudent = service.save(student);
        if (saveStudent != null)
        {
            log.log(Level.INFO, "Студент: [" + saveStudent + "] сохранен в базе данных");
        }
        return saveStudent;
    }
   
    /**
     * Метод уадаления студента из базы данных 
     * 
     * @param id
     * @param student
     * @return 
     */
    @DELETE
    
    @Path("{id}")
    @Transactional
    public void deleteStudent(@PathParam("id") Long id) {
        System.out.println("Поступил запрос на удаление id= "+ id);
        
        Student student = Student.findById(id);
        if(student != null) {
            System.out.println("Найден студент для удаления" + id);
            student.delete();
            System.out.println("Студент удален" + id);
        } else {
            System.out.println("Ошибка удаления, студент с id = ["+ id +"]не обнаружен в базе");
        }
            
        
       /* Student entity = Student.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();*/
    }
    
         

     
}

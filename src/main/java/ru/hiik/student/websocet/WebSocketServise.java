/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.student.websocet;

/**
 *
 * @author student
 */
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.server.ServerEndpoint;


import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.Session;
/**
 *
 * @author vaganovdv
 */
@ServerEndpoint("/chat/{username}")         
@ApplicationScoped
public class WebSocketServise {
     Map<String, Session> sessions = new ConcurrentHashMap<>(); 
     
     
      @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) {
        broadcast("Пользователь [" + username + "] подключился");
        sessions.put(username, session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("username") String username) {
        sessions.remove(username);
        broadcast("Пользователь [" + username + "] отключился");
    }

    @OnError
    public void onError(Session session, @PathParam("username") String username, Throwable throwable) {
        sessions.remove(username);
        broadcast("Пользователь [" + username + "] отключился в связи с ошибкой: " + throwable);
    }

    @OnMessage
    public void onMessage(String message, @PathParam("username") String username) {
          broadcast("Сообщение пользователя: [" + username + "] ==> " + message);
    }

    private void broadcast(String message) {
        sessions.values().forEach(s -> {
            s.getAsyncRemote().sendObject(message, result ->  {
                if (result.getException() != null) {
                    System.out.println("Не возможно послать сообщение: " + result.getException());
                }
            });
        });
    }
}


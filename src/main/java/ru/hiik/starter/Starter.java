/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.starter;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.transaction.Transactional;
import ru.hiik.student.Student;



/**
 *
 * @author student
 */
@ApplicationScoped
public class Starter {
    
    private static final Logger LOG = Logger.getLogger(Starter.class.getName());
    
    void onStart(@Observes StartupEvent ev) {
        LOG.info("Сервер стартовал...");
        this.creatStudent();
    }
    
    void onStop(@Observes ShutdownEvent ev) {
        LOG.info("Сервер остановился...");
        
    }
    
    
    
    
    @Transactional
    public void creatStudent() {
        Student student1 = new Student();
        student1.firstName = "Юра";
        student1.sureName = "Каплан";
        student1.lastName = "Роианович";
        student1.course = "1";
        student1.studentGroup = "Gug-32";
        System.out.println("Первый студент создан");
        
       /* Student student2 = new Student();
        student2.firstName = "Иван";
        student2.middleName = "Горемыка";
        student2.lastName = "Валерьевич";
        student2.course = "2";
        student2.studentGroup = "Повт-20Д";
        student2.persistAndFlush();
        System.out.println("Второй студент создан");*/
    }
    
    
}
